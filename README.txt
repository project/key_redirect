
Key Authentication is a plugin for Services
<http://drupal.org/project/services>.

Installation
------------

NOTE: BECAUSE THE KEY REDIRECT SERVICE EXPECTS PLAIN-TEXT USERNAMES AND
PASSWORDS, I URGE YOU TO ONLY CONNECT TO THE SERVICE VIA A SECURE SERVER (SSL)!

Installation is the same as most other Drupal modules - unpack the archive file
to your modules directory, then enable it at
<http://YOURSITE?q=admin/build/modules>.

Usage
-----
Key Authentication provides additional documentation via the Advanced Help
module <http://drupal.org/project/advanced_help>.

Author
------
Chris Yates
chris@christianyates.com
<http://christianyates.com>
Copyright 2010 Chris Yates 
and Mars Space Flight Facility
Arizona State University
